//
//  BaseRealmObject.swift
//  ZXXY LLC
//
//  Created by Jim Young on 2/1/16.
//  Copyright © 2016 Jim Young. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import ObjectMapper
import Async

class BaseRealmObject: Object, StaticMappable, RemoteObject {

    static var realmsForThreads: [String: Realm] = [:]
    static let updateQueue = DispatchQueue(label: "co.angel.realm-object-update", qos: .background)

    static var currentRealm: Realm? {
        let key = Thread.current.description
        if let realm = realmsForThreads[key] {
            return realm
        }
        else {
            let r = try! Realm()
            realmsForThreads[key] = r
            return r
        }
    }

    class func objectForMapping(map: Map) -> BaseMappable? {
        if let pk = primaryKey(), let keyValue = primaryKeyValue(properties: map.JSON) {
            if let existingObj = find(primaryKey: keyValue) {
                return existingObj
            }

            let newObj = self.init()
            newObj[pk] = keyValue
            return newObj
        }
        return nil
    }

    // Override if the key name in the JSON differs from the name of the model field.
    class func primaryJSONKey() -> String? {
        return primaryKey()
    }

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
        commonInit()
    }

    required init() {
        super.init()
        commonInit()
    }

    required init?(map: Map) {
        super.init()
        commonInit()
    }

    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
        commonInit()
    }

    override init(value: Any) {
        super.init(value: value)
        commonInit()
    }

    func commonInit() {
        // Can be used by subclasses
    }

    var currentRealm: Realm? {
        return realm ?? BaseRealmObject.currentRealm
    }

    var legit: Bool {
        return true
    }

    func mapping(map: Map) {
        if let wrealm = currentRealm {
            if wrealm.isInWriteTransaction {
                loadMap(map)
                postMap()
                if legit {
                    wrealm.add(self, update:true)
                    postSave()
                }
            }
            else {
                try! wrealm.write {
                    loadMap(map)
                    postMap()
                    if legit {
                        wrealm.add(self, update:true)
                        postSave()
                    }
                }
            }
        }
    }

    func loadMap(_ map: Map) {}

    func postMap() {}
    func postSave() {}

    class func primaryKeyValue(properties:[String:Any]) -> Any? {
        if let pk = primaryJSONKey() {
            return properties[pk]
        }
        return nil
    }

    class func primaryKeyValue<T: BaseRealmObject>(object: T) -> Any? {
        if let pk = primaryKey() {
            return object[pk]
        }
        return nil
    }

    class func find(primaryKey key:Any?, realm: Realm?=nil) -> Self? {
        if let key = key {
            let rrealm = realm ?? BaseRealmObject.currentRealm
            if let rrrealm = rrealm {
                return rrrealm.object(ofType: self, forPrimaryKey: key)
            }
        }
        return nil
    }

    class func validate(_ obj: RemoteObject) -> Bool {
        if let obj = obj as? BaseRealmObject {
            if let pk = primaryKey() {
                return obj[pk] != nil && obj.legit
            }
            else {
                return obj.legit
            }
        }
        return true
    }

    // Use this to wrap code that updates attributes:
    func update(async: Bool = false, block: @escaping ()->Void) {
        
        func doIt() {
            if let rrealm = realm ?? BaseRealmObject.currentRealm {
                if rrealm.isInWriteTransaction {
                    block()
                    if legit {
                        rrealm.add(self, update:true)
                        postSave()
                    }
                }
                else {
                    try! rrealm.write {
                        block()
                        if legit {
                            rrealm.add(self, update:true)
                            postSave()
                        }
                    }
                }
            }
        }
        
        if async {
            Async.main {
                doIt()
            }
        }
        else {
            doIt()
        }
    }
    
    // A "smarter" update- tries to perform the update off the main thread.
    func evolve<T: BaseRealmObject>(block: @escaping (T)->Void) {
        
        if let it = self as? T {
            if let rrealm = it.realm {
                if rrealm.isInWriteTransaction {
                    block(it)
                    if legit {
                        rrealm.add(self, update:true)
                        postSave()
                    }
                }
                else {
                    let ref = ThreadSafeReference(to: it)
                    BaseRealmObject.updateQueue.async {
                        if let realm = BaseRealmObject.currentRealm, let me = realm.resolve(ref) {
                            try! realm.write {
                                block(me)
                                if me.legit {
                                    realm.add(me, update:true)
                                    me.postSave()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
