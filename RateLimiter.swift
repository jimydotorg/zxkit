//
//  RateLimiter.swift
//
//  Created by Jim Young on 10/30/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation

public struct RateLimiter {

    static var timestamps: [String:Date] = [:]

    public static func limit<T>(interval:TimeInterval, tag:String?=nil, _ behavior:()->T) -> T? {
        let key = tag ?? String(describing: Thread.callStackReturnAddresses.last)
        if let lastRun = self.timestamps[key] {
            if abs(lastRun.timeIntervalSinceNow) < interval {
                return nil
            }
        }

        timestamps[key] = Date()
        return behavior()
    }

    public static func resetTag(tag:String) {
        timestamps.removeValue(forKey: tag)
    }
}
