//
//  CGRect_ZX.swift
//  ZXXY LLC
//
//  Created by Jim Young on 11/11/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation
import UIKit

// Remember:
//
//    0 ---->
//    |    +x
//    |
//    v +y

extension CGRect {

    var area: CGFloat {
        return size.area
    }

    // MARK: Some handy alternative initializers for rects

    init(x0: CGFloat, y0: CGFloat, x1: CGFloat, y1: CGFloat) {
        self = CGRect(x: x0, y: y0, width: x1-x0, height: y1-y0)
    }

    init(centerX: CGFloat, centerY: CGFloat, width: CGFloat, height: CGFloat) {
        self = CGRect(x: centerX-width/2, y: centerY-height/2, width: width, height: height)
    }

    init(rightX: CGFloat, bottomY: CGFloat, width: CGFloat, height: CGFloat) {
        self = CGRect(x: rightX-width, y: bottomY-height, width: width, height: height)
    }

    init(center: CGPoint, size: CGSize) {
        self = CGRect(centerX: center.x, centerY: center.y, width: size.width, height: size.height)
    }

    // Alternative origins:
    
    init(bottomLeftCorner start: CGPoint, size: CGSize) {
        self = CGRect(origin: CGPoint(x: start.x, y: start.y - size.height),
                      size: size)
    }

    init(bottomRightCorner start: CGPoint, size: CGSize) {
        self = CGRect(origin: CGPoint(x: start.x - size.width, y: start.y - size.height),
                      size: size)
    }

    init(topRightCorner start: CGPoint, size: CGSize) {
        self = CGRect(origin: CGPoint(x: start.x - size.width, y: start.y),
                      size: size)
    }

}
