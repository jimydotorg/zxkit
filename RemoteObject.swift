//
//  RemoteObject.swift
//
//  Created by Jim Young on 10/29/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation
import ObjectMapper

public protocol RemoteObject: class, Mappable {
    static func validate(_ obj: RemoteObject) -> Bool
}

extension RemoteObject {
    func mapping(map: Map) {}
}
