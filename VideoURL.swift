//
//  VideoURL.swift
//  imgbrd
//
//  Created by Jim Young on 5/3/16.
//  Copyright © 2016 Jim Young. All rights reserved.
//

import Foundation
import AVFoundation

// Stuff for URL's pointing to local video files
extension URL {
    
    func generateCGImages(forTimes times: [TimeInterval], completion: @escaping ([CGImage])->Void) {
        let asset = AVURLAsset(url: self)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        
        let timeValues: [NSValue] = times.map { time in
            return NSValue(time: CMTime(seconds:time, preferredTimescale: 1))
        }
        
        var results: [CGImage] = []
        generator.generateCGImagesAsynchronously(forTimes: timeValues) { (time, cgimage, actualTime, result, error) in
            if let img = cgimage {
                results.append(img)
                if results.count == times.count {
                    completion(results)
                }
            }
            else {
                completion(results)
            }
        }
    }
    
    func generateUIImages(forTimes times: [TimeInterval], completion: @escaping ([UIImage])->Void) {
        generateCGImages(forTimes: times) { cgimages in
            let uiimages = cgimages.map { cgimage in
                return UIImage(cgImage: cgimage)
            }
            completion(uiimages)
        }
    }
    
    func videoDimensions() -> CGSize? {
        let asset = AVURLAsset(url: self)
        if let videoTrack = asset.tracks(withMediaType: AVMediaTypeVideo).first {
            return videoTrack.naturalSize
        }
        return nil
    }
    
    func processVideo(preset: String,
                      filetype: String,
                      constructor: ((CALayer, CALayer, CGSize)->Void)?,
                      exportConfigurator: ((AVAssetExportSession)->Void)?,
                      completion: @escaping (URL?, Error?)->Void)
    {
        let asset = AVURLAsset(url: self)
        let mutableComposition = AVMutableComposition()
        let timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration)
        var frameRate: Int32 = 0
        
        do {
            if let videoTrack = asset.tracks(withMediaType: AVMediaTypeVideo).first {
                let compositionVideoTrack = mutableComposition.addMutableTrack(withMediaType: AVMediaTypeVideo, preferredTrackID: kCMPersistentTrackID_Invalid)
                try compositionVideoTrack.insertTimeRange(timeRange, of: videoTrack, at: kCMTimeZero)
                frameRate = Int32(videoTrack.nominalFrameRate)
            }
            
            if let audioTrack = asset.tracks(withMediaType: AVMediaTypeAudio).first {
                let compositionAudioTrack = mutableComposition.addMutableTrack(withMediaType: AVMediaTypeAudio, preferredTrackID: kCMPersistentTrackID_Invalid)
                try compositionAudioTrack.insertTimeRange(timeRange, of: audioTrack, at: kCMTimeZero)
            }
            
            if mutableComposition.tracks(withMediaType: AVMediaTypeVideo).count > 0 {
                let mutableVideoComposition = AVMutableVideoComposition()
                mutableVideoComposition.frameDuration = CMTimeMake(1, frameRate)
                
                let instruction = AVMutableVideoCompositionInstruction()
                instruction.timeRange = CMTimeRangeMake(kCMTimeZero, mutableComposition.duration)
                
                if let videoTrack = mutableComposition.tracks(withMediaType: AVMediaTypeVideo).first {
                    let passThru = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack)
                    let referenceRect = CGRect(origin: CGPoint.zero, size: videoTrack.naturalSize)
                    let newRect = referenceRect.applying(videoTrack.preferredTransform)
                    var xform = videoTrack.preferredTransform
                    xform = xform.translatedBy(x: newRect.origin.x, y: newRect.origin.y) // swap x and y?
                    let videoSize = CGSize(width: abs(newRect.size.width), height: abs(newRect.size.height))
                    mutableVideoComposition.renderSize = videoSize
                    passThru.setTransform(xform, at: kCMTimeZero)
                    
                    instruction.layerInstructions = [passThru]
                    mutableVideoComposition.instructions = [instruction]
                    
                    let parentLayer = CALayer()
                    let videoLayer = CALayer()
                    parentLayer.frame = CGRect(origin: CGPoint.zero, size: videoSize)
                    videoLayer.frame = CGRect(origin: CGPoint.zero, size: videoSize)
                    parentLayer.addSublayer(videoLayer)
                    
                    if let con = constructor {
                        con(parentLayer, videoLayer, videoSize)
                    }
                    
                    mutableVideoComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
                }
                
                if let exportSession = AVAssetExportSession(asset: mutableComposition, presetName: preset) {
                    let targetDir = NSTemporaryDirectory()
                    
                    var ext = "mov"
                    if filetype == AVFileTypeMPEG4 {
                        ext = "mp4"
                    }
                    else if filetype == AVFileTypeAppleM4V {
                        ext = "m4v"
                    }
                    
                    let moviePath = "\(targetDir)/video-\(String.random()).\(ext)"
                    try FileManager.default.removeItem(atPath: moviePath)
                    
                    exportSession.outputURL = URL(fileURLWithPath: moviePath)
                    exportSession.outputFileType = filetype
                    exportSession.videoComposition = mutableVideoComposition
                    if let exporter = exportConfigurator {
                        exporter(exportSession)
                    }
                    
                    exportSession.exportAsynchronously {
                        if exportSession.status == .completed {
                            completion(exportSession.outputURL, nil)
                            return
                        }
                        else {
                            completion(nil, exportSession.error)
                            return
                        }
                    }
                }
            }
            
            print("WTF processing video \(self)")
            completion(nil, nil)
            return
        }
        catch {
            print("error processing video \(self)")
            completion(nil, nil)
        }
    }
    
    func addWatermark(text: String?,
                      textColor: UIColor?,
                      font: UIFont?,
                      textOpacity: Float,
                      image: UIImage?,
                      imageOpacity: Float,
                      preset: String,
                      filetype: String,
                      completion: @escaping (URL?, Error?)->Void) {
        
        processVideo(
            preset: preset,
            filetype: filetype,
            constructor: { baseLayer, videoLayer, size in
                
                if let text = text {
                    let titleLayer = CATextLayer()
                    titleLayer.shadowOpacity = 0.0
                    titleLayer.borderWidth = 0.0
                    titleLayer.isWrapped = true
                    titleLayer.alignmentMode = kCAAlignmentLeft
                    titleLayer.string = text
                    if let font = font {
                        titleLayer.font = font.fontName as CFTypeRef?
                        titleLayer.fontSize = font.pointSize
                    }
                    titleLayer.foregroundColor = textColor?.cgColor
                    titleLayer.frame = CGRect(origin: CGPoint.zero, size: size)
                    titleLayer.opacity = textOpacity
                    
                    baseLayer.addSublayer(titleLayer)
                }
                
                if let image = image {
                    let imageLayer = CALayer()
                    let scaledImage = image.resized(to: size, contentMode: .scaleAspectFit)
                    imageLayer.frame = CGRect(centerX: size.width/2, centerY: size.height/2, width: scaledImage.size.width, height: scaledImage.size.height)
                    imageLayer.contents = scaledImage.cgImage
                    imageLayer.opacity = imageOpacity
                    
                    baseLayer.addSublayer(imageLayer)
                }
        },
            exportConfigurator: nil,
            completion: completion)
    }
}
