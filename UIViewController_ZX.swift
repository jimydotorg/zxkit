//
//  UIViewControllerZXtensions.swift
//  ZXXY LLC
//
//  Created by Jim Young on 11/2/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation

extension UIViewController {

    var isViewVisible: Bool {

        guard isViewLoaded else { return false }

        if let window = view.window {
            var isOnscreen: Bool
            let myRect = window.convert(window.frame, from: view.superview)
            let intersection = window.frame.intersection(myRect)
            isOnscreen = !intersection.isEmpty

            if let parent = parent as? UINavigationController {
                return isOnscreen && parent.visibleViewController == self
            }

            return isOnscreen
        }

        return false
    }
}
