//
//  BaseManagedObject.swift
//
//  Created by Jim Young on 10/15/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation
import CoreData
import ObjectMapper

protocol MogeneratedObject {
    static func entityName () -> String
    static func entity(managedObjectContext: NSManagedObjectContext!) -> NSEntityDescription!
}

@objc(BaseManagedObject)
public class BaseManagedObject : NSManagedObject, Mappable, RemoteObject {

    // Subclasses may override this to compute an object ID
    public class func idForProperties(props:[String:AnyObject]) -> String? {
        if let pid = props[idProperty] as? String {
            return pid
        }
        return nil
    }

    // Subclasses may override this to use an alternate CoreData attribute as the ID
    public class var idProperty: String {
        return "id"
    }

    public static var defaultContext: NSManagedObjectContext {
        return DataManager.shared.managedObjectContext
    }

    public static func findAll<T:BaseManagedObject>(predicate: NSPredicate, context:NSManagedObjectContext?=defaultContext) -> [T] {
        let request = NSFetchRequest(entityName: entityName())
        request.predicate = predicate
        if let result = try? context?.executeFetchRequest(request), obj = result as? [T] {
            return obj
        }
        return []
    }

    /**
     Get object by ID. Returns nil if not found.
     */
    public static func find<T:BaseManagedObject>(id objId: AnyObject?, context:NSManagedObjectContext?=defaultContext) -> T? {
        let request = NSFetchRequest(entityName: entityName())
        request.predicate = NSPredicate(format: "\(idProperty) = %s", argumentArray: [String(objId!)])
        if let result = try? context?.executeFetchRequest(request).first, obj = result as? T {
            return obj
        }
        return nil
    }

    // MARK:- Mappable protocol

    public required convenience init?(_ map: Map) {
        self.init()
        self.mapping(map)
    }

    public func mapping(map: Map) {
        assert(false, "mapping of BaseManagedObject should never be called! \(self)")
    }

    // (Not part of Mappable protocol)
    public var reverseMap: String {
        if let desc = Mapper().toJSONString(self, prettyPrint: true) {
            return desc
        }
        return ""
    }

    // MARK:- Factory methods

    public static func objectForProperties(props:[String:AnyObject], context:NSManagedObjectContext? = nil) -> Self? {
        let cont = context ?? defaultContext
        return managedObjectForProperties(props, context: cont)
    }

    public static func objectForProperties(props:[String:AnyObject]) -> Self? {
        return objectForProperties(props, context: nil)
    }

    public static func managedObjectForProperties<T:BaseManagedObject>(properties:[String:AnyObject], context:NSManagedObjectContext) -> T? {
        var props = properties
        var obj: T

        if let objId = idForProperties(properties) {
            props[idProperty] = objId
            if let existingObj: T = find(id:objId, context: context) {
                obj = existingObj
            }
            else {
                obj = self.MR_createEntityInContext(context) as! T
                obj.setPrimitiveValue(objId, forKey: idProperty)
            }
        }
        else {
            obj = self.MR_createEntityInContext(context) as! T
        }

        Mapper<T>().map(props, toObject: obj)
        return obj
    }

    public func postMap() {
        // Not sure why this function needs to be here too. Defining it in RemoteObject extension doesn't seem to work as expected
    }

}

extension BaseManagedObject: MogeneratedObject {
    public class func entityName () -> String {
        assert(false, "WTF??!?")
        return "NONE"
    }

    public class func entity(managedObjectContext: NSManagedObjectContext!) -> NSEntityDescription! {
        return NSEntityDescription.entityForName(self.entityName(), inManagedObjectContext: managedObjectContext);
    }
}

