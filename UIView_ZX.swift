//
//  Created by Jim Young on 11/1/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    // For debugging view hierarchy:
    var ancestorViews: [UIView] {
        var results: [UIView] = []
        var currentView: UIView? = self
        while let soup = currentView?.superview {
            results.append(soup)
            currentView = soup
        }
        return results
    }
    
    func subviewsOfType<T:UIView>(_ t:T.Type) ->[T] {
        var results:[T] = []
        for child in subviews {
            if let x = child as? T {
                results.append(x)
            }
            results += child.subviewsOfType(t)
        }
        return results
    }

    var rectOnscreen: CGRect {
        if let w = window {
            let myRect = w.convert(frame, from: superview)
            return w.frame.intersection(myRect)
        }
        else {
            return CGRect.zero
        }
    }

    var isOnscreen: Bool {
        return !rectOnscreen.isEmpty
    }

    var areaOnscreen: CGFloat {
        return rectOnscreen.area
    }

    var area: CGFloat {
        return bounds.size.area
    }

    var portionOnscreen: CGFloat {
        if area > 0 {
            return areaOnscreen/area
        }
        return 0.0
    }
    
    func removeGestureRecognizers(_ recursive:Bool=true) {
        if let gestures = gestureRecognizers {
            for gesture in gestures {
                removeGestureRecognizer(gesture)
            }
        }

        if recursive {
            for view in subviews {
                view.removeGestureRecognizers(true)
            }
        }
    }

    // Generates an UIImage from view:
    // http://stackoverflow.com/questions/4334233/how-to-capture-uiview-to-uiimage-without-loss-of-quality-on-retina-display
    var renderedImage: UIImage? {
        guard frame.size != CGSize.zero else { return nil }

        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0.0)
        if let ctx: CGContext = UIGraphicsGetCurrentContext() {
            ctx.interpolationQuality = CGInterpolationQuality.high
            layer.render(in: ctx)
            let img = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return img
        }
        return nil
    }
}

//MARK: Layout constraint helpers

extension UIView {
    
    func equalFrameConstraints(otherView: UIView, w: CGFloat=0.0, h: CGFloat=0.0, x: CGFloat=0.0, y: CGFloat=0.0) -> [NSLayoutConstraint] {
        return [
            widthAnchor.constraint(equalTo: otherView.widthAnchor, constant: w),
            heightAnchor.constraint(equalTo: otherView.heightAnchor, constant: h),
            centerXAnchor.constraint(equalTo: otherView.centerXAnchor, constant: x),
            centerYAnchor.constraint(equalTo: otherView.centerYAnchor, constant: y),
            ]
    }
    
    func addEqualFrameConstraints(otherView: UIView, w: CGFloat=0.0, h: CGFloat=0.0, x: CGFloat=0.0, y: CGFloat=0.0) {
        addConstraints(equalFrameConstraints(otherView:otherView, w:w, h:h, x:x, y:y))
    }

    func equalSizeConstraints(otherView: UIView, w: CGFloat=0.0, h: CGFloat=0.0, x: CGFloat=0.0, y: CGFloat=0.0) -> [NSLayoutConstraint] {
        return [
            widthAnchor.constraint(equalTo: otherView.widthAnchor, constant: w),
            heightAnchor.constraint(equalTo: otherView.heightAnchor, constant: h),
            ]
    }

    func addEqualSizeConstraints(otherView: UIView, w: CGFloat=0.0, h: CGFloat=0.0, x: CGFloat=0.0, y: CGFloat=0.0) {
        addConstraints(equalSizeConstraints(otherView: otherView, w: w, h: h, x: x, y: y))
    }
}

//MARK: "Pill" shape

// Draw rounded "pill" border. Call this from within view's drawRect function
extension UIView {

    func drawPill(fill:UIColor?=nil, stroke:UIColor?=nil, strokeWidth:CGFloat=2.0, top:Bool=false, left:Bool=false, bottom:Bool=false, right:Bool=false, clip:Bool=true, inRect:CGRect?=nil) {

        let rect = inRect ?? bounds

        guard fill != nil || stroke != nil else {
            return
        }

        let path = UIBezierPath(pillRect: rect, top:top, left:left, bottom:bottom, right:right)

        if let fill = fill {
            fill.setFill()
            path.fill(with: .normal, alpha: 1.0)
        }

        if let stroke = stroke {
            let context: CGContext! = UIGraphicsGetCurrentContext()
            context.addPath(path.cgPath)

            if clip {
                context.clip()
            }

            stroke.setStroke()
            path.lineWidth = strokeWidth*2.0
            path.stroke(with: .normal, alpha: 1.0)
        }
    }
}


extension UIBezierPath {
    
    convenience init(pillRect rect:CGRect, top:Bool=false, left:Bool=false, bottom:Bool=false, right:Bool=false) {
        var rawCorners: UInt = 0
        
        if top {
            rawCorners |= UIRectCorner.topRight.rawValue | UIRectCorner.topLeft.rawValue
        }
        
        if left {
            rawCorners |= UIRectCorner.topLeft.rawValue | UIRectCorner.bottomLeft.rawValue
        }
        
        if bottom {
            rawCorners |= UIRectCorner.bottomLeft.rawValue | UIRectCorner.bottomRight.rawValue
        }
        
        if right {
            rawCorners |= UIRectCorner.topRight.rawValue | UIRectCorner.bottomRight.rawValue
        }
        
        let corners = UIRectCorner.init(rawValue: rawCorners)
        let radius = CGSize(width: rect.size.width/2, height: rect.size.height/2)
        
        self.init(roundedRect: rect, byRoundingCorners: corners, cornerRadii: radius)
    }
}

//MARK: "Arrow" shape

// Draw an arrow border/background. Call this from within view's drawRect function
extension UIView {

    func drawArrow(leftWidth:CGFloat, rightWidth:CGFloat, fill:UIColor?=nil, stroke:UIColor?=nil, strokeWidth:CGFloat=2.0, clip:Bool=true, inRect:CGRect?=nil) {

        let rect = inRect ?? bounds
        guard fill != nil || stroke != nil else {
            return
        }

        let path = UIBezierPath(arrowRect: rect, leftWidth: leftWidth, rightWidth: rightWidth)

        if let fill = fill {
            fill.setFill()
            path.fill(with: .normal, alpha: 1.0)
        }

        if let stroke = stroke, let context = UIGraphicsGetCurrentContext() {
            context.addPath(path.cgPath)

            if clip {
                context.clip()
            }

            stroke.setStroke()
            path.lineWidth = strokeWidth*2.0
            path.stroke(with: .normal, alpha: 1.0)
        }
    }

    func drawBox(fill:UIColor?=nil, stroke:UIColor?=nil, strokeWidth:CGFloat=2.0, clip:Bool=true, inRect:CGRect?=nil) {
        drawArrow(leftWidth:0.0, rightWidth:0.0, fill: fill, stroke: stroke, strokeWidth: strokeWidth, clip: clip, inRect: inRect)
    }
}

extension UIBezierPath {
    
    convenience init(arrowRect rect:CGRect, leftWidth:CGFloat, rightWidth:CGFloat) {
        self.init()
        
        let x0 = rect.origin.x
        let y0 = rect.origin.y
        
        let rectWidth = rect.size.width
        let rectHeight = rect.size.height
        
        if leftWidth >= 0 {
            move(to: CGPoint(x: x0 + leftWidth + 0.5, y: y0 + 0.5))
        }
        else {
            move(to: CGPoint(x: x0 + 0.5, y: y0 + 0.5))
        }
        
        if rightWidth >= 0 {
            addLine(to: CGPoint(x: x0+rectWidth-rightWidth-0.5, y: y0+0.5))
            addLine(to: CGPoint(x: x0+rectWidth-0.5, y: y0+rectHeight/2.0))
            addLine(to: CGPoint(x: x0+rectWidth-rightWidth-0.5, y: y0+rectHeight-0.5))
        }
        else {
            addLine(to: CGPoint(x: x0+rectWidth-0.5, y: y0+0.5))
            addLine(to: CGPoint(x: x0+rectWidth+rightWidth-0.5, y: y0+rectHeight/2))
            addLine(to: CGPoint(x: x0+rectWidth-0.5, y: y0+rectHeight-0.5))
        }
        
        if leftWidth >= 0 {
            addLine(to: CGPoint(x: x0+leftWidth+0.5, y: y0+rectHeight-0.5))
            addLine(to: CGPoint(x: x0+0.5, y: y0+rectHeight/2))
        }
        else {
            addLine(to: CGPoint(x: x0+0.5, y: y0+rectHeight-0.5))
            addLine(to: CGPoint(x: x0-leftWidth+0.5, y: y0+rectHeight/2))
        }
        
        close()
    }
}

//MARK: Rhomboid

// Draw rounded "pill" border. Call this from within view's drawRect function
extension UIView {
    
    func drawRhomboid(fill:UIColor?=nil, stroke:UIColor?=nil, strokeWidth:CGFloat=2.0, xOffset:CGFloat=10.0, yOffset:CGFloat=0.0, clip:Bool=true, inRect:CGRect?=nil) {
        
        let rect = inRect ?? bounds
        
        guard fill != nil || stroke != nil else {
            return
        }
        
        let path = UIBezierPath(rhomboidRect: rect, xOffset: xOffset, yOffset: yOffset, fitRect: true)
        
        if let fill = fill {
            fill.setFill()
            path.fill(with: .normal, alpha: 1.0)
        }
        
        if let stroke = stroke, let context = UIGraphicsGetCurrentContext() {
            context.addPath(path.cgPath)
            
            if clip {
                context.clip()
            }
            
            stroke.setStroke()
            path.lineWidth = strokeWidth*2.0
            path.stroke(with: .normal, alpha: 1.0)
        }
    }
}

extension UIBezierPath {
    
    convenience init(rhomboidRect:CGRect, xOffset:CGFloat=10.0, yOffset:CGFloat=0.0, fitRect:Bool=false) {
        self.init()

        let rect = fitRect ? rhomboidRect.insetBy(dx: abs(xOffset), dy: abs(yOffset)) : rhomboidRect
        let rectWidth = rect.size.width
        let rectHeight = rect.size.height
        
        let halfX = xOffset/2.0
        let halfY = yOffset/2.0
        
        move(to: CGPoint(x: rect.origin.x + halfX, y: rect.origin.y + halfY))
        addLine(to: CGPoint(x: rect.origin.x + rectWidth + halfX, y: rect.origin.y - halfY))
        addLine(to: CGPoint(x: rect.origin.x + rectWidth - halfX, y: rect.origin.y + rectHeight - halfY))
        addLine(to: CGPoint(x: rect.origin.x - halfX, y: rect.origin.y + rectHeight + halfY))
        close()
    }
}



