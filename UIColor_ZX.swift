//
//  UIColor_ZX.swift
//  ZXXY LLC
//
//  Created by Jim Young on 11/6/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation

struct UIColorRGBComponents {
    var red: CGFloat
    var green: CGFloat
    var blue: CGFloat
    var alpha: CGFloat
}

struct UIColorHSBComponents {
    var hue: CGFloat
    var saturation: CGFloat
    var brightness: CGFloat
    var alpha: CGFloat
}

extension UIColor {
    
    var rgba: UIColorRGBComponents {
        
        let red = UnsafeMutablePointer<CGFloat>.allocate(capacity: 1)
        let green = UnsafeMutablePointer<CGFloat>.allocate(capacity: 1)
        let blue = UnsafeMutablePointer<CGFloat>.allocate(capacity: 1)
        let alpha = UnsafeMutablePointer<CGFloat>.allocate(capacity: 1)
        
        self.getRed(red, green: green, blue: blue, alpha: alpha)
        let result = UIColorRGBComponents(red: red.pointee,
                                          green: green.pointee,
                                          blue: blue.pointee,
                                          alpha: alpha.pointee)
        
        red.deinitialize()
        red.deallocate(capacity: 1)
        green.deinitialize()
        green.deallocate(capacity: 1)
        blue.deinitialize()
        blue.deallocate(capacity: 1)
        alpha.deinitialize()
        alpha.deallocate(capacity: 1)
        
        return result
    }
    
    var hsba: UIColorHSBComponents {
        
        let hue = UnsafeMutablePointer<CGFloat>.allocate(capacity: 1)
        let saturation = UnsafeMutablePointer<CGFloat>.allocate(capacity: 1)
        let brightness = UnsafeMutablePointer<CGFloat>.allocate(capacity: 1)
        let alpha = UnsafeMutablePointer<CGFloat>.allocate(capacity: 1)
        
        self.getHue(hue, saturation: saturation, brightness: brightness, alpha: alpha)
        let result = UIColorHSBComponents(hue: hue.pointee,
                                          saturation: saturation.pointee,
                                          brightness: brightness.pointee,
                                          alpha: alpha.pointee)
        
        hue.deinitialize()
        hue.deallocate(capacity: 1)
        saturation.deinitialize()
        saturation.deallocate(capacity: 1)
        brightness.deinitialize()
        brightness.deallocate(capacity: 1)
        alpha.deinitialize()
        alpha.deallocate(capacity: 1)
        
        return result
    }
    
    convenience init(hex: Int, alpha: CGFloat = 1.0) {
        let red = CGFloat((hex & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((hex & 0xFF00) >> 8) / 255.0
        let blue = CGFloat((hex & 0xFF)) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    convenience init(color: UIColor, alpha: CGFloat = 1.0) {
        let rgba = color.rgba
        self.init(red: rgba.red, green: rgba.green, blue: rgba.blue, alpha:alpha)
    }
    
    // http://stackoverflow.com/a/33397427
    convenience init?(hexAlphaString: String?) {
        if let hexAlphaString = hexAlphaString {
            var hex: String = hexAlphaString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
            var int = UInt32()
            Scanner(string: hex).scanHexInt32(&int)
            let a, r, g, b: UInt32
            switch hex.characters.count {
            case 3: // RGB (12-bit)
                (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
            case 6: // RGB (24-bit)
                (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
            case 8: // ARGB (32-bit)
                (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
            default:
                (a, r, g, b) = (1, 1, 1, 0)
            }
            self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
        }
        else {
            return nil
        }
    }
    
    var hex: UInt {
        var red: CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue: CGFloat = 0.0
        var alpha: CGFloat = 0.0
        
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        red = round(red*255.0)
        green = round(green*255.0)
        blue = round(blue*255.0)
        alpha = round(alpha*255.0)
        return (UInt(red) << 16 | UInt(green) << 8 | UInt(blue))
    }

    var hexAlpha: UInt {
        var red: CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue: CGFloat = 0.0
        var alpha: CGFloat = 0.0
        
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        red = round(red*255.0)
        green = round(green*255.0)
        blue = round(blue*255.0)
        alpha = round(alpha*255.0)
        return (UInt(alpha) << 24 | UInt(red) << 16 | UInt(green) << 8 | UInt(blue))
    }
    
    var hexString: String {
        return String(format: "%06x", hex)
    }
 
    var hexAlphaString: String {
        return String(format: "%08x", hexAlpha)
    }
    
    var complement: UIColor {
        let rgba = self.rgba
        return UIColor(red:(1-rgba.red), green:(1-rgba.green), blue:(1-rgba.blue), alpha:rgba.alpha)
    }
    
    func lighten(percent: CGFloat) -> UIColor {
        let hsba = self.hsba
        if hsba.alpha == 0.0 {
            return UIColor.clear
        }
        else {
            return UIColor(
                hue: hsba.hue,
                saturation: hsba.saturation,
                brightness: min(1.0, hsba.brightness*(1+percent/100.0)),
                alpha: hsba.alpha)
        }
    }
    
    func darken(percent: CGFloat) -> UIColor {
        let hsba = self.hsba
        if hsba.alpha == 0.0 {
            return UIColor.clear
        }
        else {
            return UIColor(hue: hsba.hue,
                           saturation: hsba.saturation,
                           brightness: max(0.0, hsba.brightness*(1-percent/100.0)),
                           alpha: hsba.alpha)
        }
    }
    
    func desaturate(percent: CGFloat) -> UIColor {
        let hsba = self.hsba
        if hsba.alpha == 0.0 {
            return UIColor.clear
        }
        else {
            return UIColor(hue: hsba.hue,
                           saturation: max(0.0, hsba.saturation*(1-percent/100.0)),
                           brightness: hsba.brightness,
                           alpha: hsba.alpha)
        }
    }
    
    func saturate(percent: CGFloat) -> UIColor {
        let hsba = self.hsba
        if hsba.alpha == 0.0 {
            return UIColor.clear
        }
        else {
            return UIColor(hue: hsba.hue,
                           saturation: min(1.0, hsba.saturation*(1+percent/100.0)),
                           brightness: hsba.brightness,
                           alpha: hsba.alpha)
        }
    }
    
}
