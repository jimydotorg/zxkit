//
//  UIScrollView_ZX.swift
//  ZXXY LLC
//
//  Created by Jim Young on 12/6/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation

extension UIScrollView {

    func scrollToTop(_ animated:Bool) {
        let offset = CGPoint.zero
        self.setContentOffset(offset, animated: animated)
    }

    func scrollToBottom(_ animated:Bool) {
        let offset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        self.setContentOffset(offset, animated: animated)
    }
}
