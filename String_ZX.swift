//
//  String_ZX.swift
//  ZXXY LLC
//
//  Created by Jim Young on 2/3/16.
//  Copyright © 2016 Jim Young. All rights reserved.
//

import Foundation

// Class methods
extension String {

    static func random(length: Int = 32, bag: String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") -> String! {
        var randomString = ""
        for _ in 0..<length {
            let index: Int = Int(arc4random_uniform(UInt32(bag.characters.count)))
            let randomOffset = bag.index(bag.startIndex, offsetBy: index)
            let charString = String(bag[randomOffset])
            randomString = randomString.appending(charString)
        }
        return randomString
    }
}

// Computed properties
extension String {

    var trimmed: String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    var md5: String {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CUnsignedInt(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)

        CC_MD5(str!, strLen, result)

        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }

        result.deinitialize()
        result.deallocate(capacity: digestLen)

        return String(format: hash as String)
    }

    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }

    var isValidUrl: Bool {
        if let url = NSURL(string: self), let scheme = url.scheme {
            return scheme != ""
        }
        return false
    }

    var acronym: String {
        let words = components(separatedBy: CharacterSet.whitespaces)
        var result = ""
        for word in words {
            if let char = word.uppercased().characters.first {
                result.append(char)
            }
        }
        return result
    }
}
