//
//  ZXWebView.swift
//  ALConnect
//
//  Created by Jim Young on 10/23/16.
//  Copyright © 2016 Jim Young. All rights reserved.
//

import Foundation
import WebKit

class ZXWebView: WKWebView {
    
    func requestWithSessionCookies(_ request: URLRequest) -> URLRequest {
        
        if let baseUrl = request.url?.baseURL {
            var newRequest = request
            var cookieStrings: [String] = []
            let storage = HTTPCookieStorage.shared
            if let cookies = storage.cookies(for: baseUrl) {
                for cookie in cookies {
                    cookieStrings.append("\(cookie.name)=\(cookie.value)")
                }
            }
            
            newRequest.setValue(cookieStrings.joined(separator: ";"), forHTTPHeaderField: "Cookie")
            return newRequest
        }
        return request
    }
    
    override func load(_ request: URLRequest) -> WKNavigation? {
        return(super.load(requestWithSessionCookies(request)))
    }
    
    func loadUrlString(_ urlString: String) -> WKNavigation? {
        if let url = URL(string: urlString) {
            return load(URLRequest(url: url))
        }
        return nil
    }
}
