//
//  CarouselViewController.swift
//
//  Created by Jim Young on 11/2/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class CarouselViewController: UIViewController {

    @IBOutlet var carousel: iCarousel?

    dynamic var currentController: UIViewController?=nil
    var initialIndex: Int=0

    var lastController: UIViewController?=nil
    var lastAppearingControllers: [UIViewController] = []
    
    var currentIndex: Int! {
        return carousel?.currentItemIndex
    }
    
    /**
     Subclasses must call super.viewDidLoad()
     */
    override func viewDidLoad() {
        super.viewDidLoad()

        if carousel == nil {
            let newCarousel = iCarousel(frame: view.bounds)
            view.addSubview(newCarousel)
            carousel = newCarousel
            newCarousel.snp.makeConstraints { make in
                make.edges.equalTo(view.snp.edges)
            }
        }

        carousel?.isPagingEnabled = true
        carousel?.bounces = false
        carousel?.delegate = self
        carousel?.dataSource = self
    }

    override func viewDidLayoutSubviews() {
        if controllers.count > 0 && carousel?.numberOfVisibleItems == 0 {
            carousel?.reloadData()
            carousel?.currentItemIndex = initialIndex
            currentController = controllers[initialIndex]
        }
        super.viewDidLayoutSubviews()
    }

    var controllers: [UIViewController] = [] {
        didSet {
            var hasChanges = false
            for child in childViewControllers {
                if !controllers.contains(child) {
                    child.removeFromParentViewController()
                    hasChanges = true
                }
            }

            for vc in controllers {
                if !childViewControllers.contains(vc) {
                    addChildViewController(vc)
                    hasChanges = true
                }
            }

            // Reload if needed
            if hasChanges {
                self.carousel?.reloadData()
                if let cc = currentController {
                    scrollTo(controller: cc)
                }
            }
        }
    }
    
    var scrollCallbacks: [()->Void] = []

    func scrollTo(controller:UIViewController, animated: Bool=true, completion: (()->Void)?=nil) {
        if carousel == nil || carousel?.numberOfVisibleItems == 0 {  // carousel view not yet loaded
            if let target = controllers.index(of: controller) {
                initialIndex = target
            }
        }
        else if let target = carousel?.index(ofItemView: controller.view) {
            if target != NSNotFound {
                if animated {
                    if let completion = completion {
                        scrollCallbacks.append(completion)
                    }
                    carousel?.scrollToItem(at: target, animated: true)
                }
                else {
                    carousel?.scrollToItem(at: target, animated: false)
                    completion?()
                }

            }
        }
        else {
            debugPrint("unable to set active controller: \(controller)")
        }
    }
}

extension CarouselViewController: iCarouselDataSource {

    func numberOfItems(in carousel: iCarousel) -> Int {
        return childViewControllers.count
    }

    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        if let child = controllers[index].view {
            child.frame = carousel.bounds
            return child
        }
        return UIView()
    }
}

extension CarouselViewController: iCarouselDelegate {

    func carouselDidScroll(_ carousel: iCarousel) {

        let appearing = childViewControllers.filter({ (vc) -> Bool in
                return vc.isViewVisible
            })

        for vc in appearing {
            if !lastAppearingControllers.contains(vc) {
                vc.beginAppearanceTransition(true, animated: false)
                // HACK: couldn't figure out how to avoid double invocation of viewDidAppear upon initial presentation carousel view controller,
                // so wrap this within if statement
                if lastController != nil {
                    vc.endAppearanceTransition()
                }
                else {
                    lastController = vc
                }
            }
        }

        for vc in lastAppearingControllers {
            if !appearing.contains(vc) {
                vc.beginAppearanceTransition(false, animated: false)
                vc.endAppearanceTransition()
            }
        }

        lastAppearingControllers = appearing
    }

    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        let lastCallbacks = scrollCallbacks
        scrollCallbacks.removeAll()
        for callback in lastCallbacks {
            callback()
        }
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        guard carousel.currentItemIndex >= 0 else { return }

        let newController = childViewControllers[carousel.currentItemIndex]
        if currentController != newController {
            lastController = currentController
            currentController = newController
        }
    }
}
