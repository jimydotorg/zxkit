//
//  ZXSegues.swift
//  ZXXY LLC
//
//  Created by Jim Young on 2/11/16.
//  Copyright © 2016 Jim Young. All rights reserved.
//

//  Random custom segues

import Foundation

class NavigationPushSegue: UIStoryboardSegue {

    override func perform() {
        let fromVC = source
        let toVC = destination

        if var vcs = fromVC.navigationController?.viewControllers {
            vcs.append(toVC)
            fromVC.navigationController?.setViewControllers(vcs, animated: true)
        }
        else {
            print("Can't perform PUSH segue. No navigation controller found for \(fromVC)")
        }
    }
}


class NavigationReplaceSegue: UIStoryboardSegue {

    override func perform() {
        let fromVC = source
        let toVC = destination

        if var vcs = fromVC.navigationController?.viewControllers {
            vcs.removeLast()
            vcs.append(toVC)
            fromVC.navigationController?.setViewControllers(vcs, animated: true)
        }
        else {
            print("Can't perform REPLACE segue. No navigation controller found for \(fromVC)")
        }
    }
}
