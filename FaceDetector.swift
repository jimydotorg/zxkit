//
//  FaceDetector.swift
//  ZXXY LLC
//
//  Created by Jim Young on 11/4/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation
import Async
import AVFoundation
import CoreImage

class FaceDetector {

    var detector: CIDetector! = CIDetector(ofType: CIDetectorTypeFace, context: CIContext(options: nil), options: [CIDetectorAccuracy:CIDetectorAccuracyHigh])

    func faceFeatures(_ image: CIImage, options: [String: Any]?=nil) -> [CIFaceFeature]? {
        var features: [CIFaceFeature] = []
        for feature in detector.features(in: image, options: options) {
            if feature.type == CIFeatureTypeFace {
                features.append(feature as! CIFaceFeature)
            }
        }

        return features.count > 0 ? features : nil
    }

    func faceFeatures(_ image: UIImage, options: [String: Any]?=nil) -> [CIFaceFeature]? {
        if let ciimage = image.ciImage {
            return faceFeatures(ciimage, options: options)
        }
        else if let ciimage = CIImage(image: image) {
            return faceFeatures(ciimage, options: options)
        }
        return nil
    }

    func faceFeatures(_ image: CGImage, options: [String: Any]?=nil) -> [CIFaceFeature]? {
        return faceFeatures(CIImage(cgImage: image), options: options)
    }

    func findInVideo(_ asset: AVAsset, atTimes:[CMTime]?=nil, options: [String: Any]?=nil, withResult completion:@escaping (Bool, CGImage?, [CIFaceFeature]?, CMTime)->Void) {

        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true

        let times = atTimes ?? [kCMTimeZero]
        let timeValues: [NSValue] = times.map { time in
            return NSValue(time: time)
        }

        var faceImage: CGImage?
        var faceTime: CMTime = kCMTimeInvalid
        var faceFeatures: [CIFaceFeature]?
        var count = 0

        generator.generateCGImagesAsynchronously(forTimes: timeValues) { (time, cgimage, actualTime, result, error) in
            count += 1
            if faceImage == nil, let cgi = cgimage{
                if let features = self.faceFeatures(cgi, options: options), features.count > 0 {
                    faceImage = cgimage
                    faceTime = actualTime
                    faceFeatures = features
                }
            }

            if count == times.count {
                completion(faceImage != nil, faceImage, faceFeatures, faceTime)
            }
        }
    }
}

extension UIImage {
    func faceFeatures(options: [String: Any]?=nil) -> [CIFaceFeature]? {
        return FaceDetector().faceFeatures(self, options: options)
    }
}

extension AVAsset {
    func findFace(atTimes times:[CMTime]?=nil, options: [String: Any]?=nil, withResult completion: @escaping (Bool, CGImage?, [CIFaceFeature]?, CMTime)->Void) {
        FaceDetector().findInVideo(self, atTimes: times, withResult: completion)
    }
}
