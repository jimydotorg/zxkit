//
//  NSDate_ZX.swift
//

import Foundation
import DateTools

extension Date {

    public var timeAgoInsta: String {

        let delta = -1*timeIntervalSinceNow
        let minute = 60.0
        let hour = 3600.0
        let day = hour*24.0
        let week = day*7.0
        let year = day*365.0

        if delta > year {
            let years = Int(delta/year)
            return "\(years)Y"
        }

        if delta > 4*week {
            let weeks = Int(delta/week)
            return "\(weeks)w"
        }

        if delta > 2.0*day {
            let days = Int(delta/day)
            return "\(days)d"
        }

        if delta > hour {
            let hours = Int(delta/hour)
            return "\(hours)h"
        }

        if delta > minute {
            let minutes = Int(delta/minute)
            return "\(minutes)m"
        }

        let sec = Int(delta)
        return "\(sec)s"
    }
    
}
