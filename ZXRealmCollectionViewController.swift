//
//  ZXRealmCollectionViewController.swift
//  ZXXY LLC
//
//  Created by Jim Young on 5/7/16.
//  Copyright © 2016 Jim Young. All rights reserved.
//

import Foundation
import CoreData
import Realm
import RealmSwift
import Async

// Cousin of ZXFetchedCollectionViewController, but for Realm objects

protocol ZXRealmCollectionDataSource: class {
    func object(at index: Int) -> Object?
    func index(of object: Object) -> Int?
    func numberOfItems() -> Int
    func allObjects() -> [Object]
    var notificationDelegate: ZXRealmNotificationDelegate? { get set }
    var realm: Realm? { get }
}

protocol ZXRealmNotificationDelegate: class {
    func realmDidInit(_ source: ZXRealmCollectionDataSource)
    func realmDidUpdate(_ source: ZXRealmCollectionDataSource, deletions: [Int], insertions: [Int], modifications: [Int])
    func realmDidError(_ source: ZXRealmCollectionDataSource, error: Error)
}

extension ZXRealmNotificationDelegate {
    func realmDidError(_ source: ZXRealmCollectionDataSource, error: Error) {}
}

class ZXRealmResultsController<T:Object>: ZXRealmCollectionDataSource, CustomStringConvertible {
    
    var description: String {
        let addr = Unmanaged.passUnretained(self).toOpaque()
        if let del = notificationDelegate {
            return "ZXRealmResultsController#\(addr) [\(T.self) -> \(del)]"
        }
        return "ZXRealmResultsController#\(addr) [\(T.self) -> nil]"
    }
    
    convenience init(results fetchResults: Results<T>, delegate: ZXRealmNotificationDelegate? = nil) {
        self.init()
        results = fetchResults
        notificationDelegate = delegate
    }
    
    var results: Results<T>? {
        get {
            return _results
        }

        set(newResults) {

            // DO NOT set results while within a realm write transaction.
            // Realm will throw exception: 'Cannot create asynchronous query while in a write transaction'
            if newResults?.realm?.isInWriteTransaction == true {
                print("Attempting to set results for \(self) during realm write transaction. Bad!")
            }
            
            if newResults != _results {
                detachNotifications()
                if let rez = newResults {
                    attachNotifications(rez)
                }
                _results = newResults
            }
        }
    }
    var _results: Results<T>?
    
    var realm: Realm? {
        return _results?.realm
    }
    
    func object(at index: Int) -> Object? {
        return results?[index]
    }

    func index(of object: Object) -> Int? {
        if let typedObject = object as? T {
            return results?.index(of: typedObject)
        }
        return nil
    }

    func numberOfItems() -> Int {
        if let rez = results {
            return rez.count
        }
        return 0
    }

    func allObjects() -> [Object] {
        var objects: [T] = []
        if let rez = results {
            for obj in rez {
                objects.append(obj)
            }
        }
        return objects
    }

    var notificationToken: NotificationToken? = nil
    weak var notificationDelegate: ZXRealmNotificationDelegate? {
        didSet {
            if notificationDelegate !== oldValue {
                detachNotifications()
                if let rez = results {
                    attachNotifications(rez)
                }
            }
        }
    }
    
    func attachNotifications(_ results: Results<T>) {

        notificationToken = results.addNotificationBlock { [weak self] (changes: RealmCollectionChange) in
            if let sself = self, let del = sself.notificationDelegate {
                switch changes {
                case .initial:
                    del.realmDidInit(sself)
                    break
                case .update(_, let deletions, let insertions, let modifications):
                    del.realmDidUpdate(sself, deletions: deletions, insertions: insertions, modifications: modifications)
                    break
                case .error(let error):
                    del.realmDidError(sself, error: error)
                    break
                }
            }
            else {
                NSLog("FYI: no notification delegate for \(self)")
            }
        }
    }
    
    func detachNotifications() {
        notificationToken?.stop()
    }
    
    deinit {
        results = nil
    }
}

class ZXRealmCollectionViewController: UIViewController, UICollectionViewDataSource, ZXRealmNotificationDelegate {

    @IBOutlet var collectionView: UICollectionView!

    var realmDataSource: ZXRealmCollectionDataSource? {
        didSet {
            if realmDataSource !== oldValue {
                oldValue?.notificationDelegate = nil
                realmDataSource?.notificationDelegate = self
            }
        }
    }
    
    /**
     The section of the collection view that houses the realm results
     */
    var resultSection: Int = 0

    /**
     The item index at which the results start being presented in the collection view
     */
    var resultIndexOffset: Int = 0

    /**
     Convert Realm results index to UICollectionView index path.
     */
    func toCollectionPath(_ index: Int) -> IndexPath {
        return IndexPath(item: index+resultIndexOffset, section: resultSection)
    }

    /**
     Convert UICollectionView index path to Realm results index. By default, result resides in section 0 (change by setting resultSection var)
     */
    func toResultIndex(_ collectionPath: IndexPath?) -> Int? {
        if let cp = collectionPath {
            if cp.section == resultSection && cp.item - resultIndexOffset >= 0 {
                return cp.item - resultIndexOffset
            }
        }
        return nil
    }

    // Some convenience methods
    
    // Returns nil if index out of bounds
    func realmObjectAtIndexPath(_ indexPath: IndexPath) -> Object? {
        if let rds = realmDataSource, let index = toResultIndex(indexPath), index < numberOfRealmItems {
            return rds.object(at: index)
        }
        return nil
    }

    func indexPathOfObject(_ object: Object) -> IndexPath? {
        if let index = realmDataSource?.index(of: object) {
            return toCollectionPath(index)
        }
        return nil
    }

    var numberOfRealmItems: Int {
        if let rds = realmDataSource {
            return rds.numberOfItems()
        }
        return 0
    }

    var maximumVisibleIndex: Int? {
        guard collectionView != nil else {
            return nil
        }
        var maxIndex: Int = -1
        let referencePath = toCollectionPath(0)
        for path in collectionView.indexPathsForVisibleItems {
            if path.section == referencePath.section && path.row >= maxIndex {
                maxIndex = path.row
            }
        }
        return maxIndex >= 0 ? maxIndex : nil
    }

    /**
     Calculate how far the current view is from the end of the current result.
     (Useful for determining when to load more results from a remote API.)
     */
    var currentDistanceFromEnd: Int? {
        if let maxIndex = maximumVisibleIndex {
            if let num = realmDataSource?.numberOfItems() {
                let distance = num - maxIndex - 1
                return distance >= 0 ? distance : nil
            }
        }
        return nil
    }

    // This assumes that the results section is the last section. Obviously this will not
    // always be the case.
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return resultSection + 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == resultSection {
            if let rds = realmDataSource {
                return rds.numberOfItems()+resultIndexOffset
            }
            return resultIndexOffset
        }
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        assert(false, "is collectionView:cellForItemAtIndexPath overridden in \(self)?")
        return UICollectionViewCell()
    }

    var _izReloading: Bool = false // dumb guard against calling reloadResults from within resultsDidChange
    
    func loadResults() {
        guard collectionView != nil else {
            print("WARNING: nil collectionView on \(self). Friendly suggestion: try to set realmDataSource after view is loaded")
            return
        }
        
        guard collectionView.dataSource != nil else {
            print("?!? collectionView has no dataSource! \(self)")
            return
        }

        if !_izReloading {
            _izReloading = true
            let savedDelegate = realmDataSource?.notificationDelegate
            realmDataSource?.notificationDelegate = nil
            // ^^^ hopefully this prevents further change notifications from being delivered. (this hasn't been verified to work)
            dispatchWillChange(realmDataSource)
            collectionView.reloadSections(IndexSet(integer: resultSection))
            dispatchDidChange(realmDataSource)
            realmDataSource?.notificationDelegate = savedDelegate
            _izReloading = false
        }
    }
    
    func realmDidInit(_ source: ZXRealmCollectionDataSource) {
        dispatchWillChange(source)
        collectionView?.reloadSections(IndexSet(integer: self.resultSection))
        dispatchDidChange(source)
    }

    func realmDidUpdate(_ source: ZXRealmCollectionDataSource, deletions: [Int], insertions: [Int], modifications: [Int]) {
        dispatchWillChange(source)
        if collectionView != nil {
            collectionView.performBatchUpdates({ [weak self] in
                if let wself = self {
                    wself.collectionView.deleteItems(at: deletions.map { wself.toCollectionPath($0) })
                    wself.collectionView.insertItems(at: insertions.map { wself.toCollectionPath($0) })
                    wself.collectionView.reloadItems(at: modifications.map { wself.toCollectionPath($0) })
                }
            }) { [weak self] (finished) -> Void in
                self?.dispatchDidChange(source)
            }
        }
        else {
            dispatchDidChange(source)
        }
    }
    
    func realmDidError(_ source: ZXRealmCollectionDataSource, error: Error) {
        NSLog("realmDidError \(self) \(error)")
    }

    func dispatchWillChange(_ source: ZXRealmCollectionDataSource?) {
        if let source = source, source === realmDataSource {
            resultsWillChange(source)
        }
    }
    
    func dispatchDidChange(_ source: ZXRealmCollectionDataSource?) {
        if let source = source, source === realmDataSource {
            resultsDidChange(source)
        }
    }
    
    func resultsWillChange(_ source: ZXRealmCollectionDataSource) {}
    func resultsDidChange(_ source: ZXRealmCollectionDataSource) {}
}
