//
//  CGSize_ZX.swift
//  ZXXY LLC
//
//  Created by Jim Young on 2/27/16.
//  Copyright © 2016 Jim Young. All rights reserved.
//

import Foundation
import UIKit

extension CGSize {

    var area: CGFloat {
        return width * height
    }
    
    var integral: CGSize {
        let frame = CGRect(origin: CGPoint.zero, size: self)
        return frame.integral.size
    }
}
