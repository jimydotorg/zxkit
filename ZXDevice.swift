//
//  DeviceInfo.swift
//  WhatsGood
//
//  Created by Jim Young on 10/30/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

public class ZXDevice {

    static var this = ZXDevice()

    lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()

    lazy var cacheDirectory: URL = {
        let urls = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()

    lazy var deviceId = UIDevice.current.identifierForVendor?.uuidString

    lazy var isPhone = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone

    lazy var isPad = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad

    lazy var canSMS = MFMessageComposeViewController.canSendText() &&
            UIApplication.shared.canOpenURL(URL(fileURLWithPath:"tel://4156803288"))

    lazy var canEmail = MFMailComposeViewController.canSendMail() &&
            UIApplication.shared.canOpenURL(URL(fileURLWithPath: "mailto:"))

    lazy var language: String! = NSLocale.preferredLanguages.first

    lazy var osVersion: String! = UIDevice.current.systemVersion

    lazy var appVersion: String! = {
        return CFBundleGetValueForInfoDictionaryKey(CFBundleGetMainBundle(), kCFBundleVersionKey) as! String
    }()

}
