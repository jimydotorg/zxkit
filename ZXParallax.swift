//
//  ZXParallax.swift
//  ZXXY LLC
//
//  Created by Jim Young on 3/2/16.
//  Copyright © 2016 Jim Young. All rights reserved.
//

import Foundation

protocol ParallaxProtocol {
    func setParallax(offset point: CGPoint, ratio: CGPoint)
}

extension UIViewController {
    func updateParallax(collectionView: UICollectionView!, scale: CGFloat=1.0) {
        let bounds = collectionView.bounds
        
        var views: [UICollectionReusableView] = collectionView.visibleCells
        views.append(contentsOf: collectionView.visibleSupplementaryViews(ofKind: UICollectionElementKindSectionHeader))
        views.append(contentsOf: collectionView.visibleSupplementaryViews(ofKind: UICollectionElementKindSectionFooter))

        for cell in views {
            if let pv = cell as? ParallaxProtocol {

                // Translate to context of main view
                let collectionCenter: CGPoint! = collectionView.superview?.convert(collectionView.center, to: view)
                let cellCenter: CGPoint! = cell.superview?.convert(cell.center, to: view)

                let offset: CGPoint = CGPoint(x: collectionCenter.x - cellCenter.x,
                                              y: collectionCenter.y - cellCenter.y)

                let ratio = CGPoint(x: 2.0 * offset.x / bounds.width,
                                    y: 2.0 * offset.y / bounds.height)
                
                let parallaxOffset = CGPoint(x: scale * ratio.x * cell.bounds.width,
                                             y: scale * ratio.y * cell.bounds.height)
                
                pv.setParallax(offset: parallaxOffset, ratio: ratio)
            }
        }
    }
}
