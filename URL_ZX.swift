//
//  NSURL_ZX.swift
//  ZXXY LLC
//
//  Created by Jim Young on 11/5/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation

extension URL {

    /**
     The query paramters as a dictionary.
     */
    var queryDict: [String:String] {
        var result: [String:String] = [:]
        if let items = URLComponents(url: self, resolvingAgainstBaseURL: true)?.queryItems {
            for c in items {
                result[c.name] = c.value ?? ""
            }
        }
        return result
    }
}
