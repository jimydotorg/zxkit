//
//  ApplicationNotificationListener.swift
//
//  Created by Jim Young on 11/10/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//
//
//  Easy way to listen for NSNotifications.

import Foundation
import UIKit

class NotificationListener: NSObject {

    var closures: [String:[(Notification)->Void]] = [:]

    override init() {}

    func listen(_ name: String, object: Any?, activity: @escaping (Notification)->Void) {
        if closures[name] != nil {
            closures[name]?.append(activity)
        }
        else {
            closures[name] = [activity]
        }

        let nname: Notification.Name = Notification.Name(rawValue: name)
        NotificationCenter.default.addObserver(self, selector: #selector(NotificationListener.onNotification(_:)), name: nname, object: object)
    }

    // @objc needed to be used as selector
    @objc func onNotification(_ notification: Notification) {
        if let array = closures[notification.name.rawValue] {
            for closure in array {
                closure(notification)
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

class ApplicationNotificationListener: NotificationListener {
    func listen(name: String, activity: @escaping (Notification)->Void) {
        super.listen(name, object: UIApplication.shared, activity: activity)
    }
}
