//
//  Alamofire_ZX.swift
//  ZXXY LLC
//
//  Created by Jim Young on 11/18/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

//  Extensions to Alamofire

import Foundation
import Alamofire

extension MultipartFormData {

    func append(params: [String: Any]) {
        for (key, value) in params {
            var data: Data?
            let encoding = String.Encoding.utf8
            switch value {
            case let date as Date:
                data = String(date.timeIntervalSince1970).data(using: encoding)
            case let params as [String: Any]:
                _ = try? data = JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions(rawValue:0))
            case let str as String:
                data = str.data(using: encoding)
            default:
                let str = String(describing: value)
                data = str.data(using: encoding)
            }

            if let data = data {
                append(data, withName: key)
            }
        }
    }
}
