//
//  CollectionShuffler.swift
//  ZXXY LLC
//
//  Created by Jim Young on 12/25/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation

/**
 From: http://stackoverflow.com/questions/24026510/how-do-i-shuffle-an-array-in-swift
*/

extension Collection {
    /// Return a copy of `self` with its elements shuffled
    func shuffle() -> [Generator.Element] {
        var list = Array(self)
        list.shuffleInPlace()
        return list
    }
}

extension MutableCollection where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffleInPlace() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }

        // "count" returns an IndexDistance
        // use "underestimatedCount" to get an (approximate) int
        let ucount = underestimatedCount
        for i in 0 ..< ucount - 1 {
            let j = Int(arc4random_uniform(UInt32(ucount - i))) + i
            guard i != j else { continue }
            swap(&self[i], &self[j])
        }
    }
}
