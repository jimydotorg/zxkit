//
//  ZXOutlineLabel.swift
//  ZXXY LLC
//
//  Created by Jim Young on 2/28/16.
//  Copyright © 2016 Jim Young. All rights reserved.
//

import Foundation

class ZXOutlineLabel: UILabel {

    @IBInspectable var outlineWidth: CGFloat = 0.0
    @IBInspectable var outlineColor: UIColor?

    override func drawText(in rect: CGRect) {
        let savedShadowOffset = shadowOffset
        let savedTextColor = textColor

        let c: CGContext! = UIGraphicsGetCurrentContext()

        c.setTextDrawingMode(.fill);
        shadowOffset = CGSize.zero
        super.drawText(in: rect)

        if outlineColor != nil {
            textColor = outlineColor
        }

        if outlineWidth > 0.0 {
            c.setLineWidth(outlineWidth)
            c.setLineJoin(CGLineJoin.round)
            c.setTextDrawingMode(.stroke);
            shadowOffset = CGSize.zero
            super.drawText(in: rect)
        }

        textColor = savedTextColor
        shadowOffset = savedShadowOffset
    }
}
