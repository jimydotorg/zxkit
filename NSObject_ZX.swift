//
//  NSObject_ZX.swift
//  ALConnect
//
//  Created by Jim Young on 5/5/17.
//  Copyright © 2017 Jim Young. All rights reserved.
//

import Foundation

extension NSObject {
    var pointerString: String {
//        return String(format: "%p", self)
        return "\(Unmanaged.passUnretained(self).toOpaque())"
    }
}
