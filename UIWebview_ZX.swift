//
//  UIWebview_ZX.swift
//  ZXXY LLC
//
//  Created by Jim Young on 11/6/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation

extension UIWebView {

    /*!
    @method cookies
    @abstract Returns array of cookies associated with the current request
    */
    var cookies: [HTTPCookie]? {
        if let url = self.request?.url {
            let storage = HTTPCookieStorage.shared
            if let cookies = storage.cookies(for: url) as [HTTPCookie]! {
                return cookies
            }
        }
        return nil
    }
}
