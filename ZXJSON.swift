//
//  ZXJSON.swift
//  imgbrd
//
//  Created by Jim Young on 5/8/16.
//  Copyright © 2016 Jim Young. All rights reserved.
//

import Foundation

extension Dictionary where Key: ExpressibleByStringLiteral, Value: ExpressibleByStringLiteral {

    func asJsonString() -> String? {
        if let dict = (self as Any) as? Dictionary<String, String> {
            do {
                let data = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions(rawValue: UInt.allZeros))
                if let string = String(data: data, encoding: String.Encoding.utf8) {
                    return string
                }
            } catch {
                print(error)
            }
        }
        return nil
    }

}

extension Array where Element: ExpressibleByStringLiteral {

    func asJsonString() -> String? {
        if let ary = (self as Any) as? Array<String> {
            do {
                let data = try JSONSerialization.data(withJSONObject: ary, options: JSONSerialization.WritingOptions(rawValue: UInt.allZeros))
                if let string = String(data: data, encoding: String.Encoding.utf8) {
                    return string
                }
            } catch {
                print(error)
            }
        }
        return nil
    }

}

extension String {

    func dictFromJson() -> [String: String]? {
        if let data = self.data(using: String.Encoding.utf8, allowLossyConversion: true) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]
            } catch {
                print(error)
            }
        }
        return nil
    }

    func stringArrayFromJson()-> [String]? {
        if let data = self.data(using: String.Encoding.utf8, allowLossyConversion: true) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String]
            } catch {
                print(error)
            }
        }
        return nil
    }
}
