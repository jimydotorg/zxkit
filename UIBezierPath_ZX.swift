//
//  UIBezierPath_ZX.swift
//  ZXXY LLC
//
//  Created by Jim Young on 1/6/16.
//  Copyright © 2016 Jim Young. All rights reserved.
//

//  Adapted from https://github.com/ZevEisenberg/ZEPolygon/blob/master/UIBezierPath%2BZEPolygon.m

import Foundation

extension UIBezierPath {

    static func pathWithPolygonInRect(rect: CGRect, numberOfSides: Int) -> UIBezierPath {
        var sides = numberOfSides
        if sides < 3 {
            sides = 3
        }

        let xRadius = Double(rect.width/2)
        let yRadius = Double(rect.height/2)
        let centerX = Double(rect.midX)
        let centerY = Double(rect.midY)

        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))

        for x in 0...sides {
            let theta = Double(x) * 2*M_PI / Double(sides) + M_PI/2
            let xCoord = centerX + xRadius*cos(theta)
            let yCoord = centerY + yRadius*sin(theta)
            let point = CGPoint(x: xCoord, y: yCoord)
            path.addLine(to: point)
        }

        return path
    }
}
