//
//  Created by Jim Young on 11/6/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

// Misc stuff for use with ObjectMapper

import Foundation
import ObjectMapper

public class EpochDateTransform: TransformType {
    public typealias Object = Date
    public typealias JSON = String

    public init() {}

    public func transformFromJSON(_ value: Any?) -> Date? {
        if let timeInt = value as? Double {
            return Date(timeIntervalSince1970: TimeInterval(timeInt))
        }
        else if let timeString = value as? String, let floaty = Float(timeString) {
            return Date(timeIntervalSince1970: TimeInterval(floaty))
        }

        return nil
    }

    public func transformToJSON(_ value: Date?) -> String? {
        if let date = value {
            return String(Int(date.timeIntervalSince1970))
        }
        return nil
    }
}

public class ECMADateTransform: TransformType {
    public typealias Object = Date
    public typealias JSON = String

    let dateFormat = "YYYY'-'MM'-'dd'T'HH':'mm':'SS.SSS'Z'"
    let dateFormatWithoutDecimalSeconds = "YYYY'-'MM'-'dd'T'HH':'mm':'SS'Z'"
    let timeZone = TimeZone(abbreviation: "UTC")
    public init() {}

    public func transformFromJSON(_ value: Any?) -> Date? {

        if let timeString = value as? String {
            let formatter = DateFormatter()
            formatter.timeZone = timeZone

            formatter.dateFormat = dateFormat
            if let date = formatter.date(from: timeString) {
                return date
            }

            formatter.dateFormat = dateFormatWithoutDecimalSeconds
            if let date = formatter.date(from: timeString) {
                return date
            }
        }

        return nil
    }

    public func transformToJSON(_ value: Date?) -> String? {
        if let date = value {
            let formatter = DateFormatter()
            formatter.dateFormat = dateFormat
            formatter.timeZone = timeZone
            return formatter.string(from: date)
        }
        return nil
    }
}


public class JSONSerializeTransform: TransformType {
    public typealias Object = Data
    public typealias JSON = String

    public init() {}

    public func transformFromJSON(_ value: Any?) -> Data? {
        if let v = value {
            let data = try? JSONSerialization.data(withJSONObject: v, options: JSONSerialization.WritingOptions(rawValue: 0))
            return data
        }
        return nil
    }

    public func transformToJSON(_ value: Data?) -> String? {
        if let value = value {
            return String(data: value, encoding: .utf8)
        }
        else {
            return nil
        }
    }
}


public class IntStringTransform: TransformType {
    public typealias Object = String
    public typealias JSON = Int

    public init() {}

    public func transformFromJSON(_ value: Any?) -> String? {
        if let v = value {
            return String(describing: v)
        }
        return nil
    }

    public func transformToJSON(_ value: String?) -> Int? {
        if let value = value {
            return Int(value)
        }
        return nil
    }
}


public class IntTransform: TransformType {
    public typealias Object = Int
    public typealias JSON = Int
    
    public init() {}
    
    public func transformFromJSON(_ value: Any?) -> Int? {
        if let v = value as? Int {
            return v
        }
        else if let v = value as? String {
            return Int(v)
        }
        return nil
    }
    
    public func transformToJSON(_ value: Int?) -> Int? {
        return value
    }
}
