//
//  ZXPageViewController.swift
//  ZXXY LLC
//
//  Created by Jim Young on 11/9/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation

/**
 Unfortunately it's not possible (at least not easily) to disable bounce on UIPageViewController
*/
class ZXPageViewController: UIPageViewController {

    var _initialController: UIViewController?

    override init(transitionStyle style: UIPageViewControllerTransitionStyle, navigationOrientation: UIPageViewControllerNavigationOrientation, options: [String : Any]? = nil) {
        super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: options)
        self.dataSource = self
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.dataSource = self
    }

    var controllers: [UIViewController] = [] {
        didSet {
            // If the currently active controller is no longer included..
            var newController: UIViewController?
            viewControllers?.forEach { vc in
                if !controllers.contains(vc) {
                    if let newVc = pageViewController(self, viewControllerBefore: vc) {
                        newController = newController ?? newVc
                    }
                    else {
                        newController = newController ?? pageViewController(self, viewControllerAfter: vc)
                    }
                }
            }

            if let newVc = newController {
                setActiveController(newVc, animated:false)
            }
        }
    }

    func setActiveController(_ controller:UIViewController, animated: Bool=true, completion: ((Bool) -> Void)?=nil) {
        if viewControllers == nil || viewControllers!.count == 0 {
            _initialController = controller
        }
        else if !viewControllers!.contains(controller) {
            setViewControllers(
                [controller],
                direction: UIPageViewControllerNavigationDirection.forward,
                animated: animated,
                completion:completion
            )
        }
    }

    override func viewWillLayoutSubviews() {
        if (viewControllers == nil || viewControllers!.count == 0) && controllers.count > 0 {
            let controller = _initialController ?? controllers[0]
            setViewControllers(
                [controller],
                direction: UIPageViewControllerNavigationDirection.forward,
                animated: false,
                completion:nil
            )
        }
    }
}

extension ZXPageViewController: UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let index = controllers.index(of: viewController) {
            let nextIndex = index + 1
            if nextIndex < controllers.count {
                return controllers[nextIndex]
            }
        }
        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let index = controllers.index(of: viewController) {
            let prevIndex = index - 1
            if prevIndex >= 0 {
                return controllers[prevIndex]
            }
        }
        return nil
    }
}
