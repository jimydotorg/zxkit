//
//  UIImage_ZX.swift
//  ZXXY LLC
//
//  Created by Jim Young on 11/17/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation

extension UIImage {

    // Returns a file path URL
    func toJpegFile(quality: CGFloat=0.7) -> URL? {
        if let data = UIImageJPEGRepresentation(self, quality) {
            let key = "\(NSUUID().uuidString).jpg"
            let temp = URL(fileURLWithPath: key, relativeTo: ZXDevice.this.cacheDirectory)
            FileManager.default.createFile(atPath: temp.path, contents: data, attributes: nil)
            return temp
        }
        return nil
    }

    // Returns a file path URL
    func toPngFile() -> URL? {
        if let data = UIImagePNGRepresentation(self) {
            let key = "\(NSUUID().uuidString).png"
            let temp = URL(fileURLWithPath: key, relativeTo: ZXDevice.this.cacheDirectory)
            FileManager.default.createFile(atPath: temp.path, contents: data, attributes: nil)
            return temp
        }
        return nil
    }

    func maskWithImage(_ mask: UIImage, size: CGSize) -> UIImage {
        if size.equalTo(CGSize.zero) {
            return UIImage()
        }

        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, mask.scale)
        mask.draw(in: rect, blendMode: CGBlendMode.normal, alpha: 1.0)

        let scaledImage = self.resized(to: size, contentMode: .scaleAspectFill, quality: .high)
        scaledImage.draw(in: rect, blendMode: CGBlendMode.sourceIn, alpha: 1.0)

        let context = UIGraphicsGetCurrentContext()!
        let maskedImageRef = context.makeImage()
        UIGraphicsEndImageContext()
        return UIImage(cgImage: maskedImageRef!)
    }

    func maskWithImage(_ mask: UIImage) -> UIImage {
        return maskWithImage(mask, size: self.size)
    }

    static func blankImage(size: CGSize, color: UIColor = UIColor.clear) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        color.set()
        let context = UIGraphicsGetCurrentContext()!
        context.interpolationQuality = CGInterpolationQuality.high
        context.fill(CGRect(origin: CGPoint.zero, size: size))
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }

    static func tintedImage(filename: String, color: UIColor) -> UIImage? {
        if let mask = UIImage(named: filename) {
            let image = self.blankImage(size: mask.size, color: color).maskWithImage(mask)
            if mask.scale > 1.0 {
                return UIImage(cgImage: image.cgImage!, scale: mask.scale, orientation: mask.imageOrientation)
            }
            else {
                return image
            }
        }
        return nil
    }

    func overlayWith(image overlay: UIImage, inRect rect: CGRect) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        // draw self
        draw(in: CGRect(origin: CGPoint.zero, size: size))
        // draw overlay
        overlay.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }

    func tinted(color: UIColor) -> UIImage {
        let img = UIImage.blankImage(size: size, color: color).maskWithImage(self)
        if scale > 1.0 {
            return UIImage(cgImage: img.cgImage!, scale: scale, orientation: imageOrientation)
        }
        return img
    }
}

//
//  UIImage+Resize.swift
//
//  Originally created by Trevor Harmon on 8/5/09
//  Translated to Swift by Daniel Park on 2/5/15.
//  Further modifications for Swift 2 by jimy
//
//  Free for personal or commercial use, with or without modification
//  No warranty is expressed or implied

import Foundation
import UIKit

extension UIImage {

    // Returns a copy of this image that is cropped to the given bounds.
    // The bounds will be adjusted using CGRectIntegral.
    // This method ignores the image's imageOrientation setting.
    func cropped(to bounds: CGRect) -> UIImage {
        let imageRef: CGImage! = self.cgImage!.cropping(to: bounds)
        return UIImage(cgImage: imageRef)
    }

    // Make it so that neither width or height are larger than dim.
    // Returns self if already small enough. (i.e., image is never enlarged)
    func shrink(to dim: CGFloat, quality: CGInterpolationQuality = .high) -> UIImage {

        let ratio = size.width/size.height
        let bounds = ratio > 1.0 ? CGSize(width: dim, height: dim/ratio) : CGSize(width: dim*ratio, height: dim)
        
        if bounds.area < size.area {
            return self.resized(to: bounds.integral,
                                contentMode: .scaleAspectFit,
                                quality: quality)
        }
        else {
            return self
        }
    }

    // Like shrink, but won't refuse to enlarge.
    func scale(to dim: CGFloat, quality: CGInterpolationQuality = .high) -> UIImage {
        
        let ratio = size.width/size.height
        let bounds = ratio > 1.0 ? CGSize(width: dim, height: dim/ratio) : CGSize(width: dim*ratio, height: dim)
        
        return self.resized(to: bounds.integral,
                            contentMode: .scaleAspectFit,
                            quality: quality)
    }


    func stretched(to newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        draw(in: CGRect(origin: CGPoint.zero, size: newSize))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage!
    }

    // Expands the dimensions of the image without resizing image. 
    // (akin to enlarging canvas size in Photoshop)
    func extend(to newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        draw(in: CGRect(centerX: newSize.width/2, centerY: newSize.height/2, width: size.width, height: size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }

    // Returns a resized copy of the image, taking into account its orientation
    // The image will be scaled disproportionately if necessary to fit the bounds specified by the parameter
    func resized(to newSize: CGSize, quality: CGInterpolationQuality = .high) -> UIImage {
        var transpose: Bool

        switch(self.imageOrientation) {
        case .left:
            fallthrough
        case .leftMirrored:
            fallthrough
        case .right:
            fallthrough
        case .rightMirrored:
            transpose = true
            break
        default:
            transpose = false
            break
        }

        return self.resized(
            to: newSize,
            transform: self.transformForOrientation(newSize),
            transpose: transpose,
            quality: quality
        )
    }

    func resized(to bounds: CGSize,
                 contentMode: UIViewContentMode,
                 quality: CGInterpolationQuality = .high) -> UIImage {
        
        let horizontalRatio: CGFloat = bounds.width / self.size.width
        let verticalRatio: CGFloat = bounds.height / self.size.height
        var ratio: CGFloat = 1

        switch(contentMode) {
        case .scaleAspectFill:
            ratio = max(horizontalRatio, verticalRatio)
            break
        case .scaleAspectFit:
            ratio = min(horizontalRatio, verticalRatio)
            break
        default:
            print("Unsupported content mode \(contentMode)")
        }

        let newSize:CGSize = CGSize(width: self.size.width * ratio, height: self.size.height * ratio)
        return self.resized(to: newSize, quality: quality)
    }

    func resized(to newSize:CGSize,
                 transform:CGAffineTransform,
                 transpose:Bool,
                 quality: CGInterpolationQuality = .high) -> UIImage {
        
        let newRect:CGRect = CGRect(origin: CGPoint.zero, size: newSize).integral
        let transposedRect: CGRect = CGRect(origin: CGPoint.zero, size: newRect.size)
        let imageRef = self.cgImage

        let contextInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue | CGBitmapInfo.byteOrder32Little.rawValue)

        // build a context that's the same dimensions as the new size
        let bitmap: CGContext! = CGContext(
            data: nil,
            width: Int(newRect.size.width),
            height: Int(newRect.size.height),
            bitsPerComponent: imageRef!.bitsPerComponent,
            bytesPerRow: 0,
            space: CGColorSpaceCreateDeviceRGB(),
            bitmapInfo: contextInfo.rawValue
        )

        // rotate and/or flip the image if required by its orientation
        bitmap.concatenate(transform)

        // set the quality level to use when rescaling
        bitmap.interpolationQuality = quality

        // draw into the context; this scales the image
        bitmap.draw(imageRef!, in: transpose ? transposedRect : newRect)

        // get the resized image from the context and a UIImage
        let newImageRef = bitmap.makeImage()
        let newImage = UIImage(cgImage: newImageRef!)

        return newImage
    }

    func transformForOrientation(_ newSize:CGSize) -> CGAffineTransform {
        var transform:CGAffineTransform = CGAffineTransform.identity
        switch (self.imageOrientation) {
        case .down:          // EXIF = 3
            fallthrough
        case .downMirrored:  // EXIF = 4
            transform = transform.translatedBy(x: newSize.width, y: newSize.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            break
        case .left:          // EXIF = 6
            fallthrough
        case .leftMirrored:  // EXIF = 5
            transform = transform.translatedBy(x: newSize.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
            break
        case .right:         // EXIF = 8
            fallthrough
        case .rightMirrored: // EXIF = 7
            transform = transform.translatedBy(x: 0, y: newSize.height)
            transform = transform.rotated(by: -CGFloat(M_PI_2))
            break
        default:
            break
        }

        switch(self.imageOrientation) {
        case .upMirrored:    // EXIF = 2
            fallthrough
        case .downMirrored:  // EXIF = 4
            transform = transform.translatedBy(x: newSize.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored:  // EXIF = 5
            fallthrough
        case .rightMirrored: // EXIF = 7
            transform = transform.translatedBy(x: newSize.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            break
        default:
            break
        }

        return transform
    }
    
}

extension UIImage {

    func mirror() -> UIImage {
        return UIImage(cgImage: self.cgImage!, scale: self.scale, orientation: .leftMirrored)
    }
}
