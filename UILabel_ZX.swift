//
//  UILabel_ZX.swift
//  ALConnect
//
//  Created by Jim Young on 11/29/16.
//  Copyright © 2016 Jim Young. All rights reserved.
//

import Foundation

extension UILabel {
    
    func fontSizeToFit(size: CGSize, minFontSize: CGFloat = 8.0, maxFontSize: CGFloat = 400.0) -> CGFloat {
        
        guard maxFontSize > minFontSize else { return minFontSize }
        guard size.height > 0.0 else { return minFontSize }
        guard size.width > 0.0 else { return minFontSize }
        
        if let txt = text {
            let fontSize = maxFontSize
            let tryFont = font.withSize(fontSize)
            let constraintSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
            let nsstring = NSString(string: txt)
            let trySize = nsstring.boundingRect(with: constraintSize,
                                                options: .usesLineFragmentOrigin,
                                                attributes: [NSFontAttributeName: tryFont],
                                                context: nil).size

            if trySize.height <= size.height && trySize.width <= size.width {
                return fontSize
            }
            else {
                let widthOverage = max((trySize.width - size.width)/size.width, 0)
                let heightOverage = max((trySize.height - size.height)/size.height, 0)
                let maxOverage = max(widthOverage, heightOverage)
                let delta = fontSize * (maxOverage - 1) / maxOverage
                let newFontSize = fontSize - max(1, delta)
                return fontSizeToFit(size: size, minFontSize: minFontSize, maxFontSize: newFontSize)
            }
        }
        return 0
    }
}
