//
//  NSData_ZX.swift
//  ZXXY LLC
//
//  Created by Jim Young on 11/19/15.
//  Copyright © 2015 Jim Young. All rights reserved.
//

import Foundation

extension Data {

    func UTF8String() -> String? {
        return String(data: self, encoding: .utf8)
    }

}
